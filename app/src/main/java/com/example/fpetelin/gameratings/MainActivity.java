package com.example.fpetelin.gameratings;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private Spinner platform_spinner;
    private EditText gameTitle;
    private PopupWindow mPopupWindow;
    private RelativeLayout mRelativeLayout;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen);
        mContext = getApplicationContext();

        gameTitle = (EditText)findViewById(R.id.enterTitleInput);

        platform_spinner = (Spinner) findViewById(R.id.platform_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.platform_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        platform_spinner.setAdapter(adapter);
        try {
            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            // Get private mPopup member variable and try cast to ListPopupWindow
            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(platform_spinner);

            // Set popupWindow height to 1000px
            popupWindow.setHeight(1000);
        }
        catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            // silently fail...
        }

        final RequestQueue queue = Volley.newRequestQueue(this);

        mRelativeLayout = (RelativeLayout) findViewById(R.id.rl);
        Button clickButton = (Button) findViewById(R.id.search_button);
        clickButton.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v){
                // Initialize a new instance of LayoutInflater service
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);

                // Inflate the custom layout/view
                final View customView = inflater.inflate(R.layout.custom_layout,null);

                final String gameTitletext = WordUtils.capitalize(gameTitle.getText().toString());
                final String gamePlatformtext = platform_spinner.getSelectedItem().toString();

                //show toast text of the search
                Toast.makeText(MainActivity.this, "You searched for " + gameTitletext + " on " + gamePlatformtext,
                        Toast.LENGTH_SHORT).show();
                String gameTitleparameter = gameTitletext.replaceAll(" ", "+");
                String requesturl = "https://videogamesrating.p.mashape.com/get.php?count=5&game="+gameTitleparameter;

                //pull data from IGN
                StringRequest stringRequest = new StringRequest(Request.Method.GET, requesturl,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    //obtain first matching result's title and score
                                    JSONArray myarray = new JSONArray(response);
                                    String title, score, thumbnail;

                                    boolean exact = false;
                                    //search through the JSON array for an exact title match,
                                    //if not found return the first result
                                    title = myarray.getJSONObject(0).getString("title");
                                    score = myarray.getJSONObject(0).getString("score");
                                    thumbnail = myarray.getJSONObject(0).getString("thumb");
                                    for (int i = 0; i < myarray.length(); i++){
                                        if (gameTitletext.equals(myarray.getJSONObject(i).getString("title"))){
                                            title = myarray.getJSONObject(i).getString("title");
                                            score = myarray.getJSONObject(i).getString("score");
                                            thumbnail = myarray.getJSONObject(i).getString("thumb");
                                        }
                                    }

                                    //obtain available platforms and search for match with user-specified one
                                    JSONObject jsonplats = myarray.getJSONObject(0).getJSONObject("platforms");
                                    boolean pfound = false;
                                    for (int i = 1; i <= jsonplats.length(); i++){
                                        if (jsonplats.getString(String.valueOf(i)).equals(gamePlatformtext)){
                                            pfound = true;
                                        }
                                    }
                                    if (pfound && !score.isEmpty()){
                                        mPopupWindow = new PopupWindow(
                                                customView,
                                                LayoutParams.WRAP_CONTENT,
                                                LayoutParams.WRAP_CONTENT,
                                                true // true flag sets focus to popup, so clicking outside closes the window
                                        );

                                        // Set an elevation value for popup window
                                        // Call requires API level 21
                                        if(Build.VERSION.SDK_INT>=21){
                                            mPopupWindow.setElevation(20.0f);
                                        }

                                        // Get a reference for the custom view close button
                                        ImageButton closeButton = (ImageButton) customView.findViewById(R.id.ib_close);

                                        // Set a click listener for the popup window close button
                                        closeButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                mPopupWindow.dismiss();
                                            }
                                        });

                                        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER,0,0);
                                        // Set text output of result
                                        TextView tresult = (TextView) mPopupWindow.getContentView().findViewById(R.id.tv);
                                        tresult.setText("Found game for "+gamePlatformtext+": \n\n" + title + "\n\nIGN score: " + score+"/10");
                                        // Get access to ImageView
                                        ImageView ivImage = (ImageView) mPopupWindow.getContentView().findViewById(R.id.fullimage);
                                        // Fire async request to load image
                                        Picasso.with(customView.getContext()).load(thumbnail).fit().into(ivImage);
                                    }
                                    else if (!pfound && !score.isEmpty())
                                        //there is an entry with score - wrong platform
                                        Toast.makeText(getApplicationContext(), title + " is not available on " + gamePlatformtext, Toast.LENGTH_LONG).show();
                                    //there is an entry with no score (weird data in IGN database?) - treat as not found
                                    else Toast.makeText(getApplicationContext(), "Game not found", Toast.LENGTH_LONG).show();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Game not found", Toast.LENGTH_LONG).show();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("Didn't receive valid response");
                    }

                })
                {
                    //modify the headers of the request packet to put api key
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("X-Mashape-Key","qjFrB4cV3WmshNlbovoz1gjTElhWp1wbA2rjsn5dQqN5w0lrIn");
                    params.put("Accept", "application/json");
                    return params;
                    }
                };
                queue.add(stringRequest);


            }
        });

    }
}
